package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int middle = 0;

        int lowerbound = number.getLowerbound();
        int upperbound = number.getUpperbound();

        while (lowerbound != upperbound) {

            middle = (lowerbound + upperbound) / 2;
            System.out.println(middle);

            if (number.guess(middle) == 0) {
                return middle;
            } else if (0 < number.guess(middle)) {
                upperbound = middle;
            } else {
                lowerbound = middle;
            }
        }
        System.out.println(middle);
        return middle;
    }
}
