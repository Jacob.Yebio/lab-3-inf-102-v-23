package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {

        return findPeak(numbers, 0);


    }
    int findPeak(List<Integer> list, int index){


        int left = index -1;
        int current = index;
        int right = index + 1;
        int peakElement = 0;

        if (left < 0 || left >= list.size()) {

            left = 0;
        }
        else
            left = list.get(left);
        current = list.get(current);
        if (right < 0 || right >= list.size()) {

            right = 0;
        }
        else right = list.get(right);

        if( left <= current && current >= right){
            peakElement = current;
        }
        else{
            peakElement = findPeak(list, index +1);

        }
        return peakElement;
    }
}
