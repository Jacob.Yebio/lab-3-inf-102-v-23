package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        if (list.size() == 0) {

            return 0;

        } else {
            long firstElement = list.get(0);
            list.remove(0);
            return firstElement + sum(list);

        }
    }
}
